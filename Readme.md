# Context

This repository contains sources and pdf of the lectures, seminars and conferences 
I am giving in ecology and GIS

# Compilation

- The presentations are made using:
    - beamer-latex
    - Beamer-Torino theme (https://github.com/bbatsov/beamer-torino-theme)
- There are always 2 main tex files: 
    - one defining the class, packages, title, author... (the is the one to compile)
    - one for the content
- The pdf is in the same folder

# Content

## 2017.11_kau_seminar

- This folder contain the source and pdf of a presentation based on some results of my PhD.
- The presentation is titled: Conservation of highly endangered species does not necessarily 
lead to biodiversity conservation. 
- It is a case study of the impact of agri-environmental scheme designed for birds conservation 
on arthropods and plants

##  20180111_kau_gis_intro

- This folder contains a very short introduction to GIS for master students 
- The folder GisPractice_fauna_flora_data will be used for practice

## 20180219_OikosCongress_PosterLafage

- This folder contains the LateX sources of a poster presented in the Oikos Congress
2018 in Trondheim.
- The poster presents the first results of q meta-analysis on drivers of aquatic-terrestrial
exchanges in riparian ecosystems