\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Methods}{21}{0}{2}
\beamer@sectionintoc {3}{Results}{24}{0}{3}
\beamer@subsectionintoc {3}{1}{Cutting date}{24}{0}{3}
\beamer@subsectionintoc {3}{2}{MAE}{34}{0}{3}
\beamer@subsectionintoc {3}{3}{Landscape}{44}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{46}{0}{4}
\select@language {english}
