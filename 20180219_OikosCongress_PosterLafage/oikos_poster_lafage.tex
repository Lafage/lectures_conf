\documentclass[final,hyperref={pdfpagelabels=false}]{beamer}
\mode<presentation>
  {
  %\usetheme{Berlin}
  \usetheme{Dreuw}
  }
  \usepackage{times}
  \usepackage{amsmath,amsthm, amssymb, latexsym}
  \boldmath
  \usepackage[english]{babel}
  \usepackage[latin1]{inputenc}
  \usepackage[orientation=portrait,size=a0,scale=1.4,debug]{beamerposter}
  \usepackage{multicol}

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \graphicspath{{figures/}}
  \title{\Large Large scale drivers of aquatic/terrestrial exchanges}
  \author{Lafage D. \and Eckstein L. \and  Bergman E. \and \"{O}sterling M. \and Piccolo J.}
  \institute{NRRV, Department of Environmental and Life Sciences / Biology,\\ 
Karlstad University, Sweden}
  \date{Feb. 2018, Trondheim}


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \begin{document}
  \begin{frame}{} 
    \vfill
    \begin{columns}[t]
      \begin{column}{.48\linewidth}

\begin{block}{Introduction}
	\begin{itemize}
    	\item Cross-boundary fluxes are recognized as key components of riparian ecosystem functions 
	\item Recent development of stable isotope techniques induced large number of diet studies
	\item \textbf{We present a meta-analysis on the poorly addressed question of the impact of landscape-scale factors on cross boundary-fluxes via diet studies using stable isotopes}
	\end{itemize}         
\end{block}

\begin{block}{Theoretical background: simplified riparian food-web}
	\begin{columns}
		\begin{column}{.35\textwidth}
			\begin{itemize}
            	\item Landscape factors impact predator and prey abundance and diversity
            	\item Locally diet is influenced by:
            		\begin{itemize}
            		\item Prey availability
            		\item Predator density
            		\end{itemize}
            	\item \textbf{Do landscape factors impact predator diet?} 
			\end{itemize}
		\end{column}
        \begin{column}{.60\textwidth}
        \centering
			\includegraphics[width=0.95\linewidth]{images/trophic_web_2.png}\\
		\end{column}
	\end{columns}
\end{block}
            

\begin{block}{Method: data acquisition}
	\begin{minipage}{0.9\textwidth}
	\begin{enumerate}
	\begin{multicols}{2}
    	\item Study search:
		\begin{itemize}
        \item Web of science + Google Scholar
		\item Keys words:
			\begin{itemize}
			\item stable isotope;
			\item riparian ecosystems;
			\item diet;
			\item subsidies                				
			\end{itemize}  
		\end{itemize}
    \item Screening
    	
	\item Selection:
		\begin{itemize}
        \item Precise location of sampling sites available
		\item Isotope data usable
        \item At least 2 sampling sites
        \end{itemize}		
	\item Data extraction:
		\begin{itemize}
        \item From tables and figures
		\item From supplementary material
        	\item From authors (mailing)
        \end{itemize}
	\item Diets re-computing
		\begin{itemize}
		\item Same fractionation values
		\item Same Bayesian 2 sources (aquatic and terrestrial) mixing model 
        \end{itemize} 
        \end{multicols}         		
	\end{enumerate}
	\end{minipage}
             
	\vskip-1ex
\end{block}


            
           
            
\begin{block}{Method: GIS data extraction}
	\begin{columns}
		\begin{column}{.45\textwidth}
			\begin{itemize}
			\item Catchment computed from Digital Elevation Models
			\item Variables extraction:
				\begin{itemize}
				\item Land-use
				\item Percent tree cover
				\item Catchment area (site size effect)
				\item Riverbed width (photo-interpretation)
				\item Climate (Koppen-Geiger climate classification)
				\item Hydro-eco-region (FEOW)
				\item Population density (NASA)
				\end{itemize}
			\end{itemize}
		\end{column}
		
		\begin{column}{.50\textwidth}
			\centering
				\includegraphics[width=1\linewidth]{images/data_extract_2.png}\\
					\begin{tiny}	                     
						\textit{Extraction of land-use class area}
					\end{tiny} 
		\end{column}
	\end{columns}
		
\end{block}

\begin{block}{Method: Analysis (work in progress)}
	\begin{itemize}
	\item Bayesian Generalized Linear Mixed Models planned
	\item Frequentist Generalized Linear Mixed Model presented here
	\item Response variable: \textbf{Mean proportion of aquatic prey} in diet (here for spiders and carabid beetles)
	\item Fixed effects: \% land-use classes, mean percent tree cover, size (catchment area and river width), mean population
	\item Random effects: study, hydro-eco-region, climate
	\end{itemize}
\end{block}
             
            

\end{column}


\begin{column}{.48\linewidth}

\begin{block}{Results: Studies available}
\vskip1ex
	\begin{itemize}
	\item 44 studies available
		\begin{itemize}
		\item 20 rejected: no precise location, no usable data
		\item 21 processed until now
		\item 458 diets over 118 sampling sites
		\end{itemize}
	\end{itemize}
	\begin{center}
		\includegraphics[width= 0.95\linewidth]{images/grps_4.png} 
			\begin{tiny}
			\textit{Studies dealing with terrestrial predator diets in riparian ecosystems (only freshwater). Red symbols: studies not selected}\par
			\end{tiny}	
	\end{center}
\end{block}

\begin{block}{Results: \% aquatic prey in terrestrial predator diets (work in progress)}  
	\begin{center}
		\begin{tiny}
			\includegraphics[width= 0.95\linewidth]{images/diets_2.png} \\
				
				\textit{\% aquatic prey in diet for spiders and carabid beetles}
		\end{tiny}
	\end{center}            
\end{block}

 \vfill
\begin{block}{Results: Catchment scale drivers (work in progress)}
	\begin{itemize}
	\item Variables selected by GLMM:
		\begin{itemize}
        \item No impact of study, hydro-eco-region or climate
		\item Best model: \% aquatic prey in diet $\sim$ \% tree cover + \% agriculture
        \end{itemize}
	\end{itemize}
	\begin{center}
		\includegraphics[width= 0.75\linewidth]{images/diet_agri_tree.png}\\
			\begin{tiny}
			\textit{\% aquatic prey in diet as a function of \% tree cover and \% agriculture in catchment}              
			\end{tiny}
	\end{center}              
\end{block}
            
\vfill	


\begin{block}{Conclusions}
	\begin{itemize}
	\item \textbf{45\% studies not used} due to lack of informations (location, isotope values...)  
	\item Landscape-scale factors influence terrestrial predator diets
    \item \textbf{Forest and agriculture cover} positively influence use of aquatic subsidies
    \end{itemize}
\end{block}


      \end{column}
    \end{columns}
  \end{frame}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t
%%% End:
